﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice = "";
            List<Notebook> arr = new List<Notebook>();
            while (choice != "6")
            {
                Console.WriteLine("[1]Ввести данные");
                Console.WriteLine("[2]Редактировать данные");
                Console.WriteLine("[3]Удалить данные");
                Console.WriteLine("[4]Отобразить весь список записей");
                Console.WriteLine("[5]Отобразить весь список записей(кратко)");
                Console.WriteLine("[6]ВЫХОД");
                choice = Console.ReadLine();
                if (int.TryParse(choice, out int k))
                {
                    switch (k)
                    {
                        case 1:
                            Notebook a = new Notebook();
                            arr.Add(a);
                            Console.WriteLine("---МЕНЮ ВВОДА ДАННЫХ---");
                            Console.WriteLine("Введите фамилию:");
                            a.fio[0] = Console.ReadLine();
                            while (string.IsNullOrWhiteSpace(a.fio[0]))
                            {
                                Console.WriteLine("Поле не может быть пустым");
                                Console.WriteLine("Введите фамилию:");
                                a.fio[0] = Console.ReadLine();
                            }
                            Console.WriteLine("Введите имя:");
                            a.fio[1] = Console.ReadLine();
                            while (string.IsNullOrWhiteSpace(a.fio[1]))
                            {
                                Console.WriteLine("Поле не может быть пустым");
                                Console.WriteLine("Введите имя:");
                                a.fio[1] = Console.ReadLine();
                            }
                            Console.WriteLine("Введите отчество:");
                            a.fio[2] = Console.ReadLine();
                            Console.WriteLine("Введите страну:");
                            a.country = Console.ReadLine();
                            while (string.IsNullOrWhiteSpace(a.country))
                            {
                                Console.WriteLine("Поле не может быть пустым");
                                Console.WriteLine("Введите страну:");
                                a.country = Console.ReadLine();
                            }
                            Console.WriteLine("Введите номер телефона:");
                            a.phone = Console.ReadLine();
                            while (string.IsNullOrWhiteSpace(a.phone))
                            {
                                Console.WriteLine("Поле не может быть пустым");
                                Console.WriteLine("Введите номер телефона:");
                                a.phone = Console.ReadLine();
                            }
                            Console.WriteLine("Введите организацию:");
                            a.organization = Console.ReadLine();
                            Console.WriteLine("Введите должность:");
                            a.position = Console.ReadLine();
                            Console.WriteLine("Введите дату рождения:");
                            a.birth = Console.ReadLine();
                            Console.WriteLine("Введите заметки:");
                            a.notes = Console.ReadLine();
                            break;
                        case 2:
                            Console.WriteLine("---МЕНЮ РЕДАКТИРОВАНИЯ ДАННЫХ---");
                            int count = 0;
                            string word = "";
                            foreach (Notebook i in arr.ToArray())
                            {
                                count++;
                                word += i.id.ToString() + " ";
                            }
                            if (count == 0)
                            {
                                Console.WriteLine("Нет записей");
                                Console.WriteLine();
                                break;
                            }
                            else
                                Console.WriteLine($"Выберите запись по ID: {word}");
                            Console.WriteLine();
                            string q = Console.ReadLine();
                            if (int.TryParse(q, out int position))
                            {
                                bool ans = true;
                                foreach (Notebook y in arr.ToArray())
                                {
                                    if(y.id == position)
                                    {
                                        ans = false;
                                        Console.WriteLine($"ФИО: {arr[position].fio[0]} {arr[position].fio[1]} {(arr[position].fio[2] != "" && arr[position].fio[2] != " " ? arr[position].fio[2] : "")}{(arr[position].phone != "" && arr[position].phone != " " ? ", Номер телефона: " + arr[position].phone : "")}{(", Страна: " + arr[position].country)}{(arr[position].birth != "" && arr[position].birth != " " ? ", ДР: " + arr[position].birth : "")}{(arr[position].organization != "" && arr[position].organization != " " ? ", Организация: " + arr[position].organization : "")}{(arr[position].position != "" && arr[position].position != " " ? ", Заметки: " + arr[position].position : "")}{(arr[position].notes != "" && arr[position].notes != " " ? ", Заметки: " + arr[position].notes : "")}");
                                        Console.WriteLine("Что вы хотите отредактировать?");
                                        Console.WriteLine("[1]ФИО");
                                        Console.WriteLine("[2]Номер телефона");
                                        Console.WriteLine("[3]Страну");
                                        Console.WriteLine("[4]Организацию");
                                        Console.WriteLine("[5]Должность");
                                        Console.WriteLine("[6]Дату рождения");
                                        Console.WriteLine("[7]Заметки");
                                        Console.WriteLine("[8]Все данные");
                                        string r = Console.ReadLine();
                                        if (int.TryParse(r, out int o))
                                        {
                                            switch (o)
                                            {
                                                case 1:
                                                    Console.WriteLine("Введите фамилию:");
                                                    arr[position].fio[0] = Console.ReadLine();
                                                    while (string.IsNullOrWhiteSpace(arr[position].fio[0]))
                                                    {
                                                        Console.WriteLine("Поле не может быть пустым");
                                                        Console.WriteLine("Введите фамилию:");
                                                        arr[position].fio[0] = Console.ReadLine();
                                                    }
                                                    Console.WriteLine("Введите имя:");
                                                    arr[position].fio[1] = Console.ReadLine();
                                                    while (string.IsNullOrWhiteSpace(arr[position].fio[1]))
                                                    {
                                                        Console.WriteLine("Поле не может быть пустым");
                                                        Console.WriteLine("Введите имя:");
                                                        arr[position].fio[1] = Console.ReadLine();
                                                    }
                                                    Console.WriteLine("Введите отчество:");
                                                    arr[position].fio[2] = Console.ReadLine();
                                                    break;
                                                case 2:
                                                    Console.WriteLine("Введите номер телефона:");
                                                    arr[position].phone = Console.ReadLine();
                                                    while (string.IsNullOrWhiteSpace(arr[position].phone))
                                                    {
                                                        Console.WriteLine("Поле не может быть пустым");
                                                        Console.WriteLine("Введите номер телефона:");
                                                        arr[position].phone = Console.ReadLine();
                                                    }
                                                    break;
                                                case 3:
                                                    Console.WriteLine("Введите страну:");
                                                    arr[position].country = Console.ReadLine();
                                                    while (string.IsNullOrWhiteSpace(arr[position].country))
                                                    {
                                                        Console.WriteLine("Поле не может быть пустым");
                                                        Console.WriteLine("Введите страну:");
                                                        arr[position].country = Console.ReadLine();
                                                    }
                                                    break;
                                                case 4:
                                                    Console.WriteLine("Введите организацию:");
                                                    arr[position].organization = Console.ReadLine();
                                                    break;
                                                case 5:
                                                    Console.WriteLine("Введите должность:");
                                                    arr[position].position = Console.ReadLine();
                                                    break;
                                                case 6:
                                                    Console.WriteLine("Введите дату рождения:");
                                                    arr[position].birth = Console.ReadLine();
                                                    break;
                                                case 7:
                                                    Console.WriteLine("Заметки:");
                                                    arr[position].notes = Console.ReadLine();
                                                    break;
                                                case 8:
                                                    Console.WriteLine("Введите фамилию:");
                                                    arr[position].fio[0] = Console.ReadLine();
                                                    while (string.IsNullOrWhiteSpace(arr[position].fio[0]))
                                                    {
                                                        Console.WriteLine("Поле не может быть пустым");
                                                        Console.WriteLine("Введите фамилию:");
                                                        arr[position].fio[0] = Console.ReadLine();
                                                    }
                                                    Console.WriteLine("Введите имя:");
                                                    arr[position].fio[1] = Console.ReadLine();
                                                    while (string.IsNullOrWhiteSpace(arr[position].fio[1]))
                                                    {
                                                        Console.WriteLine("Поле не может быть пустым");
                                                        Console.WriteLine("Введите имя:");
                                                        arr[position].fio[1] = Console.ReadLine();
                                                    }
                                                    Console.WriteLine("Введите отчество:");
                                                    arr[position].fio[2] = Console.ReadLine();
                                                    Console.WriteLine("Введите номер телефона:");
                                                    arr[position].phone = Console.ReadLine();
                                                    while (string.IsNullOrWhiteSpace(arr[position].phone))
                                                    {
                                                        Console.WriteLine("Поле не может быть пустым");
                                                        Console.WriteLine("Введите номер телефона:");
                                                        arr[position].phone = Console.ReadLine();
                                                    }
                                                    Console.WriteLine("Введите страну:");
                                                    arr[position].country = Console.ReadLine();
                                                    while (string.IsNullOrWhiteSpace(arr[position].country))
                                                    {
                                                        Console.WriteLine("Поле не может быть пустым");
                                                        Console.WriteLine("Введите страну:");
                                                        arr[position].country = Console.ReadLine();
                                                    }
                                                    Console.WriteLine("Введите организацию:");
                                                    arr[position].organization = Console.ReadLine();
                                                    Console.WriteLine("Введите должность:");
                                                    arr[position].position = Console.ReadLine();
                                                    Console.WriteLine("Введите дату рождения:");
                                                    arr[position].birth = Console.ReadLine();
                                                    Console.WriteLine("Заметки:");
                                                    arr[position].notes = Console.ReadLine();
                                                    break;
                                                default:
                                                    Console.WriteLine("Нет такого поля");
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("Нет такого поля");
                                        }
                                        break;
                                    }
                                }
                                if(ans)
                                    Console.WriteLine("Нет записи с таким ID");
                                Console.WriteLine();
                            }
                            else { Console.WriteLine("Нет записи с таким ID"); Console.WriteLine(); }
                            break;
                        case 3:
                            Console.WriteLine("---МЕНЮ УДАЛЕНИЯ ДАННЫХ О ЗАПИСИ---");
                            string temp = "";
                            int cnt = 0;
                            foreach (Notebook i in arr.ToArray())
                            {
                                cnt++;
                                temp += i.id.ToString() + " ";
                            }
                            if (cnt == 0)
                            {
                                Console.WriteLine("Нет записей");
                                Console.WriteLine();
                                break;
                            }
                            else
                                Console.WriteLine($"Выберите запись по ID: {temp}");
                            string t = Console.ReadLine();

                            if (int.TryParse(t, out int pos))
                            {
                                bool ans = true;
                                foreach (Notebook obj in arr.ToArray())
                                {
                                    if (obj.id == pos)
                                    {
                                        ans = false;
                                        arr.Remove(arr[pos]);
                                        Console.WriteLine($"Запись с id {pos} удалена");
                                    }
                                }
                                if (ans)
                                    Console.WriteLine("Нет записи с таким ID");
                                Console.WriteLine();
                            }
                            else { Console.WriteLine("Нет записи с таким ID"); }
                            break;
                        case 4:
                            Console.WriteLine("---МЕНЮ ОТОБРАЖЕНИЯ СПИСКА ВСЕХ ЗАПИСЕЙ---");
                            int numb = 0;
                            foreach (Notebook i in arr.ToArray())
                            {
                                numb++;
                                Console.WriteLine($"{i.id}) ФИО: {i.fio[0]} {i.fio[1]}{(!string.IsNullOrWhiteSpace(i.fio[2]) ?(" " + i.fio[2]) : "")}{(!string.IsNullOrWhiteSpace(i.phone) ? ", Номер телефона: " + i.phone : "")}{(", Страна: " + i.country)}{(!string.IsNullOrWhiteSpace(i.birth) ? ", ДР: " + i.birth : "")}{(!string.IsNullOrWhiteSpace(i.organization) ? ", Организация: " + i.organization :"")}{(!string.IsNullOrWhiteSpace(i.position) ? ", Должность: " + i.position : "")}{(!string.IsNullOrWhiteSpace(i.notes) ? ", Заметки: " + i.notes : "")}");
                            }
                            if (numb == 0)
                            {
                                Console.WriteLine("Нет записей");
                            }
                            Console.WriteLine();
                            break;
                        case 5:
                            Console.WriteLine("---МЕНЮ ОТОБРАЖЕНИЯ СПИСКА ВСЕХ ЗАПИСЕЙ (кратко) ---");
                            int num = 0;
                            foreach (Notebook i in arr.ToArray())
                            {
                                num++;
                                Console.WriteLine($"{i.id}) ФИ: {i.fio[0]} {i.fio[1]}{(!string.IsNullOrWhiteSpace(i.phone) ? ", Номер телефона: " + i.phone : "")}");
                            }
                            if (num == 0)
                            {
                                Console.WriteLine("Нет записей");
                            }
                            Console.WriteLine();
                            break;
                        case 6:
                            Console.WriteLine("Выход из программы");
                            return;
                        default:
                            Console.WriteLine("Выберите один из предложенных вариантов");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Выберите один из предложенных вариантов");
                    Console.WriteLine();
                }
            }
        }
        class Notebook
        {
            public int id;
            public static int cnt = 0;
            public string[] fio = new string[3];
            public string phone;
            public string country;
            public string birth;
            public string organization;
            public string position;
            public string notes;
            public Notebook()
            {
                id = cnt;
                cnt++;
            }
        }

    }
}
